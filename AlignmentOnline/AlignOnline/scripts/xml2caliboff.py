#!/usr/bin/env python

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description = 'Make new caliboff database from alignment xml in standard online locations')
    parser.add_argument('versions',help='''Version to pick for subdetector (Velo, TT, IT, OT, Muon)
    es Velo:v5  IT:v6,v7 will pick v5 for both VeloGlobal and VeloModules and v6 of ITGlobal and v7 of ITModules.
    If a sub-detector is not present it will not be included in the layer
    ''', nargs='+', type=str)
    parser.add_argument('-d', '--db',help='Database to which add the layer',default='newDB.db')
    parser.add_argument('-t', '--tag',help='tag, if not given the tag name is based on the versions used')
    parser.add_argument('--kind',help='kind of database',choices=['LHCBCOND','SIMCOND','CALIBOFF'], default='CALIBOFF')
    args = parser.parse_args()
##########################

subDetectors = ['Velo', 'TT', 'IT', 'OT', 'Muon']

import os, shutil, tempfile
tmp_dir = tempfile.mkdtemp()

for block in args.versions:
    subDetector, versions = block.split(':')
    versions = versions.split(',')
    if len(versions) == 1:
        versions.append(versions[0])
    if subDetector not in subDetectors:
        print subDetector, 'not in', subDetectors, 'will be ignored'
        continue
    directory = '{tmp_dir}/Conditions/Online/{subDetector}/Alignment/'.format(**locals())
    os.makedirs(directory)
    for version, gm in zip(versions, ['Global', 'Modules']):
        inFile_name = '/group/online/alignment/{subDetector}/{subDetector}{gm}/{version}.xml'.format(**locals())
        inFile = open(inFile_name)
        print 'Added', inFile_name
        text = inFile.read()
        inFile.close()
        outFile = open(directory+gm+'.xml','w')
        outFile.write('<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"><DDDB>')
        outFile.write(text)
        outFile.write('</DDDB>')
        outFile.close()

database = "sqlite_file:{0}/{1}".format(args.db, args.kind)
os.system('copy_files_to_db.py -c '+database+' -s '+tmp_dir+'/')


if not args.tag:
    args.tag = '_'.join(args.versions)

print 'Inserting tag:', args.tag
from CondDBUI import CondDB
MasterLHCBCOND = CondDB(database, readOnly=False)
MasterLHCBCOND.recursiveTag("/", args.tag)

shutil.rmtree(tmp_dir)

