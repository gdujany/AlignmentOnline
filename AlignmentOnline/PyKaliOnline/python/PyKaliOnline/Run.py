#!/usr/bin/env python
# =============================================================================
# @file KaliCalo/MassDistribution/RunMassDistributionCalibration.py
# A way to start the calibration with the Mass Distribution Fit method
# @author Daria Savrina (dsavrina@mail.cern.ch)
# @date 2011-04-15
# =============================================================================
"""
A file which starts the full calibration process with the Mass Distribution Fit
Method.

Usage:
> python RunMassDistributionCalibration.py
"""
## General things
import os, re, random, socket

## Printouts
from AnalysisPython.Logger import getLogger

loggerIt = getLogger('Iterator:')
loggerAn = getLogger('Analyser:')

## Kali modules responsible for filling and fitting the histograms
from KaliCalo.MassDistribution.FillTask import fillDatabase
from KaliCalo.MassDistribution.Analyse import analyse

## Kali. Just Kali
import KaliCalo.Kali as Kali

## Modules to treat the output
## (histograms and calibration constants)
import KaliCalo.Kali.LambdaMap as LambdaMap
import KaliCalo.ZipShelve as zs
from KaliCalo.MassDistribution import HistoMap

## Can be used to join the cells in groups
## in case of low statistics
import KaliCalo.FakeCells as FakeCells
#from RealPrsCoefficients import *
import KaliCalo.Cells     as Cells
Zones = Cells.Zones
AZ = Cells.EcalZone
IZ = Cells.InnerZone
MZ = Cells.MiddleZone
OZ = Cells.OuterZone

## All the paths used during the calibration
## are stored here
#from PyKaliOnline.Paths import Paths, PassIt, MaxIt, loggerAn, loggerIt
from Paths import Paths, PassIt, MaxIt, n_prims, n_secs

import sys
sys.path += ['.']

import argparse
parser = argparse.ArgumentParser(description = "Input file delivery")
parser.add_argument("--trueiter"  , type = int, dest = "trueiter"  , nargs = 2, default = [])
parser.add_argument("--input-file", type = str, dest = "input_file", nargs = '*', default = None) ## list of input files
parser.add_argument("--index"     , type = int, dest = "index"     , default = 0)
parser.add_argument("--process"   , type = str, dest = "process"   , default = 'Fill')
parser.add_argument("--lambdas"   , type = str, dest = "lambdas"   , default = None)
args = parser.parse_args()

trueiter = args.trueiter
if trueiter:
    loggerAn.warning('Current iteration number: %i pass %i iteration' %(trueiter[0],trueiter[1]))

input_file = args.input_file
index      = args.index
hn         = socket.gethostname()
pt         = Paths(index, hn,)
process    = args.process

print "NUMBER OF INPUT FILES", len(input_file)

## Read the coefficients database
## if there's any
lmap = LambdaMap.LambdaMap()
if args.lambdas: lmap.read(args.lambdas)

StoredHistoPath  = pt.histos_location_it()        ## path to save the filled histogram map
StoredLambdaPath = pt.lambdas_location_it()       ## path to save the filled histogram map
StoredProblemPath= pt.problems_location_it()      ## path to save the info about problematic cells at each iteration

FittedPath       = pt.fitted_histos_location_an() ## path to save the filled histogram map
StoredFittedPath = pt.fitted_histos_location_it() ## path to save the filled histogram map

LambdasLocation  = pt.lambdas_location()          ## path to the coefficients to be used at the next step
LambdasDBPath    = pt.lambdas_db_location()       ## DBase for re-reconstruction

MergedHistoPath  = pt.merged_histos_path()        ## place for histograms after merging

def FillTheHistograms():
	## ==============================
        ## fill the histograms
	## ==============================
    histos, badfiles = fillDatabase (  ## recieve a map of filled histograms and list of incorrect files
        lmap                           ## map of the calibration coefficients
        , input_file                   ## input nTuples
        , manager = None               ## parallel processing manager
        , cellFunc = FakeCells.SameCell()
        , dbase_name = None
        )

    #hn = socket.gethostname()
    import ROOT
    from KaliCalo.Cells import CellID

    FilledHistoPath = pt.histos_location_an()   ## path to save the filled histogram map
    loggerAn.info('Saving histograms to '+FilledHistoPath)
    histos.save(FilledHistoPath)

    #rootfile = ROOT.TFile(FilledHistoPath,'recreate')
    #
    #for hk in histos.keys():
    #    dname = 'Ecal_%s_%i_%i'%(hk.areaName(),hk.row(),hk.col())
    #    thedir = rootfile.mkdir(dname)
    #    thedir.cd()
    #    for j in range(len(histos[hk])):
    #        histos[hk][j].Write()
    #    thedir.cd()
    #
    #rootfile.Close()

def mergeHistos():
    ## first join the filled histograms from all the workers
    import MergeHistos
    histos = input_file
    histos.sort()
    print '# of histograms = ',len(histos)

    loggerAn.warning('TRY TO MERGE %d histograms ' % len( histos ))
    sys.stdout.flush()

    hmap = MergeHistos.mergeDBs ( histos  , 5 )

    loggerAn.warning('RESULT Entries:%d Histos:%s' % ( hmap.entries() , len(hmap) ))

    hmap.save(MergedHistoPath)
    if MergedHistoPath in histos: histos.remove(MergedHistoPath)

    for h in histos:
        #print "Removing", h
        os.remove(h)

def SplitHistos():
    ## first join the filled histograms from all the workers
    import MergeHistos
    histos = pt.gethistomaps()
    histos = histos+pt.getmergedhistos()
    histos.sort()
    print '# of histograms = ',len(histos)

    loggerIt.warning('TRY TO MERGE %d histograms ' % len( histos ))
    #sys.stdout.flush()

    hmap = MergeHistos.mergeDBs ( histos  , 10 )

    loggerIt.warning('RESULT Entries:%d Histos:%s' % ( hmap.entries() , len(hmap) ))

    ## find the last one saved and
    ## save current as the next one
    ih   = 1
    Pass = 1
    while True:
        if ih > n_prims:
            Pass += 1
            ih    = 1
        if os.path.exists(StoredHistoPath%(Pass,ih)): ## look through all the existing files
            print StoredHistoPath%(Pass,ih), 'exists'
            ih += 1
            continue
        else:                                  ## write to the file next to the latest existing
            print 'Saving histograms to ', StoredHistoPath%(Pass,ih)
            hmap.save(StoredHistoPath%(Pass,ih))
            os.system('chmod a+rwx '+StoredHistoPath%(Pass,ih))
            break

    ## calculate a number of histograms per worker
    sys.path.insert(0,os.path.dirname(pt.files_to_merge()))
    from HistogramsForMerging import filled

    nworkers = len(filled)
    nhistos  = len(hmap.keys())
    Nmin = nhistos/nworkers
    Nmax = Nmin+1
    rst = (float(nhistos)/float(nworkers)-Nmin)*nworkers
    rst = int(rst)
    gh = 0
    group_max = HistoMap()
    group_min = HistoMap()

    loggerIt.warning("SPLITTING %s HISTOGRAMS BETWEEN %s WORKERS" %(nhistos,nworkers))
    loggerIt.warning("%s WORKERS WILL HAVE %s HISTOGRAMS PER EACH"%(rst, Nmax))
    loggerIt.warning("%s WORKERS WILL HAVE %s HISTOGRAMS PER EACH"%(nworkers-rst,Nmin))

    for hk in hmap.keys():
        if gh < (rst*Nmax):
            group_max.insert(hmap[hk])
        else:
            group_min.insert(hmap[hk])
        gh += 1

    print len(group_max),"+", len(group_min),"=", len(group_max)+len(group_min), "=", nhistos

    d1 = group_max.split(Nmax)
    d2 = group_min.split(Nmin)
    data = []
    data += d1
    data += d2

    print len(d1),"+",len(d2),"=",len(data),"=",nworkers,"?"

    ## get 'All-Ecal' histograms
    hA = hmap [ AZ ]
    ## inner area
    hI = hmap [ IZ ]
    ## middle area
    hM = hmap [ MZ ]
    ## outer area
    hO = hmap [ OZ ]

    ## To have the global histograms in each histomap
    #print len(data)
    for dt in data:
        if not hA in dt[0]: dt[0].append(hA)
        if not hI in dt[0]: dt[0].append(hI)
        if not hM in dt[0]: dt[0].append(hM)
        if not hO in dt[0]: dt[0].append(hO)
        #print len(dt[0]),"HISTOGRAMS"

    ## rewrite existing databases by the
    ## parts of the split histo map
    for ih in range(nworkers):
        splitmap = HistoMap()
        hm       = filled[ih].split('Filled')
        #print ih, data[ih][0]
        for jh in data[ih][0]:
            splitmap.insert(jh)
        #print "Clearing previous location", hm
        splitmap.save(hm[0]+'Split'+hm[1])
        
    for ih in histos:
        os.remove(ih)
    os.remove(pt.files_to_merge())
    os.remove(pt.files_to_merge()+'c')

def FitTheHistograms():
    ## ==============================
    ## fit the histograms
    ## ==============================

    hmap = HistoMap()
    hmap.read(input_file[0])

    #print "FITTING THE HISTOGRAMS"
    bad,low,nfit = analyse (         ## recieve a dictionary of the bad, not fit histograms and the ones with low statistics
        hmap              ,          ## map of the filled histograms
        lmap              ,          ## map of the calibration coefficients
        fitted  = False   ,          ## not fitted yet
        manager = None    ,          ## parallel processing manager
        #nHistos = 60                ## number of histograms per core/machine
        )

    LambdasPath     = pt.lambdas_location_an()  ## path to save the calibration coefficients
    print "Saving lambdas to ", LambdasPath
    lmap.save (LambdasPath)

    ## Print statistics
    print 'BAD-CELLS : #', len ( bad  )
    print 'LOW-CELLS : #', len ( low  )
    print 'NFIT-CELLS: #', len ( nfit )

    #print "Fitting completed"

    ProblemsPath    = pt.problems_location_an() ## path to save the info about problematic cells
    #print "Saving problematic cells info to ", ProblemsPath
    dbase = zs.open (ProblemsPath)
    dbase['BadCells'] = bad
    for b in bad :
        key = 'BadCell ' + str(b)
        dbase [key] = hmap[b]
    dbase['LowCells'] = low
    for b in low :
        key = 'LowCell ' + str(b)
        dbase [key] = hmap[b]
    dbase['NFitCells'] = nfit
    for b in nfit :
        key = 'nFitCell ' + str(b)
        dbase [key] = hmap[b]
    dbase.close()

    hmap.save(FittedPath)

    os.remove(input_file[0])

def CollectLambdas():
    ## all the coefficients databases
    ToCollect = pt.getlambdamaps()
    CollectProblems = pt.getproblems()
    CollectFitted = pt.getfittedhistos()
    PrevLams = []
    for i in range(1+n_secs):
        if LambdasLocation%i in ToCollect:
            ToCollect.remove(LambdasLocation%i)
            PrevLams.append(LambdasLocation%i)
    ToCollect.sort()
    PrevLams.sort()
    CollectFitted.sort()

    ## total database for all of them
    lmap_tot  = LambdaMap.LambdaMap()
    if PrevLams: lmap_tot.read(PrevLams[-1]); print PrevLams[-1]
    #print PrevLams, len(lmap_tot)

    for ldb in ToCollect:
        #print "Adding the coefficients from", ldb
        lmap      = LambdaMap.LambdaMap()
        lmap.read(ldb)
        for k in lmap:
            if lmap_tot.has_key(k):
                lmap_tot[k].append(lmap[k][-1]*lmap_tot[k][-1])
            else:
                lmap_tot[k].append(lmap[k][-1])
        #print "Removing", ldb
        os.remove(ldb)

    hmap_tot = HistoMap()
    for cf in CollectFitted:
        hmap_small = HistoMap()
        hmap_small.read(cf)
        for hs in hmap_small.keys():
            hmap_tot.insert(hmap_small[hs])
        os.remove(cf)

    ## find the last one saved and
    ## save current as the next one
    ih   = 1
    Pass = 1
    while True:
        #print ih, PassIt
        if ih > n_prims:
            Pass += 1
            ih    = 1
        if os.path.exists(StoredLambdaPath%(Pass,ih)): ## look through all the existing files
            print StoredLambdaPath%(Pass,ih), 'exists'
            print StoredProblemPath%(Pass,ih), 'exists'
            ih += 1
            continue
        else:                                  ## write to the file next to the latest existing
            print 'Saving coefficients to ', StoredLambdaPath%(Pass,ih)
            lmap_tot.save(StoredLambdaPath%(Pass,ih))

            print 'Saving problematic cells to ', StoredProblemPath%(Pass,ih)
            prmap_tot = zs.open(StoredProblemPath%(Pass,ih))

            hmap_tot.save(StoredFittedPath%(Pass,ih))
            os.system('chmod a+rwx '+StoredFittedPath%(Pass,ih))

            os.system('chmod a+rwx '+StoredLambdaPath%(Pass,ih))
            break

    for pdb in CollectProblems:
        #print "Adding problematic cells info from", pdb

        prmap = zs.open(pdb)
        for k in prmap.keys():
            prmap_tot [k] = prmap [k]
        prmap.close()

        os.remove(pdb)

    prmap_tot.close()
    os.system('chmod a+rwx '+StoredProblemPath%(Pass,ih))

    lmap_tot.save(LambdasLocation%Pass)
    print "Saving coefficients to ", LambdasLocation%Pass

    #lmap_tot.save(pt.lambdas_location_it())

    ## Print statistics
    #print 'BAD-CELLS : #', len ( bad  ) 
    #print 'LOW-CELLS : #', len ( low  ) 
    #print 'NFIT-CELLS: #', len ( nfit ) 
    ### print out the statistics of the coefficients variation
    large = lmap_tot.large ( 0.15 )
    print '# of >15% ' , len(large)

    large = lmap_tot.large ( 0.10 )
    print '# of >10% ' , len(large)
    
    large = lmap_tot.large ( 0.05 )
    print '# of > 5% ' , len(large)
    
    large = lmap_tot.large ( 0.02 )
    print '# of > 2% ' , len(large)
    
    large = lmap_tot.large ( 0.01 )
    print '# of > 1% ' , len(large)

    ## create the database for the re-reconstruction
    dbase = zs.open(LambdasDBPath%Pass)
    dbase['ecal'] = lmap_tot.lambdas()
    dbase.close()

def GetOutput():
    ##
    ## Create the resulting histos
    ##
    from KaliCalo.FitUtils import fitPi0, getPi0Params
    from KaliCalo.Cells import CellID
    import ROOT

    nit = n_prims

    Conv = {}
    Sigm = {}

    oh = os.path.join(pt.store_location(),"OutputHistograms.root")
    histofile = ROOT.TFile(oh,'recreate')

    for j in range(1,1+n_secs):
        MassInner  = []
        MassMiddle = []
        MassOuter  = []
        Mass       = []
        Histos     = []

        #if j == 2: nit = nit-1

        Conv['Pass%i'%j] = ROOT.TH1F('Conv%i'%j,'Convergency',nit,0,nit)
        Sigm['Pass%i'%j] = ROOT.TH1F('Sigm%i'%j,'Resolution' ,nit,0,nit)
        for i in range(1,nit+1):
        #for i in range(1,nit+1):
            Histos    .append(HistoMap())
            ni = i-1
            Histos[ni].read(StoredHistoPath%(j,i))
            MassInner .append(ROOT.TH1F('MassInnerPass%iIt%i'%(j,i) ,'Inner  mass at Pass%i Iteration N%i'%(j,i), 250, 0, 250))
            MassMiddle.append(ROOT.TH1F('MassMiddlePass%iIt%i'%(j,i),'Middle mass at Pass%i Iteration N%i'%(j,i), 250, 0, 250))
            MassOuter .append(ROOT.TH1F('MassOuterPass%iIt%i'%(j,i) ,'Outer  mass at Pass%i Iteration N%i'%(j,i), 250, 0, 250))
            Mass      .append(ROOT.TH1F('MassPass%iIt%i'%(j,i)      ,'Mass at Pass%i Iteration N%i'%(j,i)       , 250, 0, 250))

            for ck in range(0,3):
                MassInner[ni] .Add(Histos[ni][CellID('Ecal','Inner' ,31,31)][ck])
                MassMiddle[ni].Add(Histos[ni][CellID('Ecal','Middle',31,31)][ck])
                MassOuter[ni] .Add(Histos[ni][CellID('Ecal','Outer' ,31,31)][ck])
            Mass[ni].Add(MassInner[ni])
            Mass[ni].Add(MassMiddle[ni])
            Mass[ni].Add(MassOuter[ni])

            fitPi0(Mass[ni])
            mp = getPi0Params(Mass[ni])
            print "All:",mp
            Conv['Pass%i'%j].SetBinContent(i,mp[1].value())
            Conv['Pass%i'%j].SetBinError  (i,mp[1].error())

            Sigm['Pass%i'%j].SetBinContent(i,mp[2].value())
            Sigm['Pass%i'%j].SetBinError  (i,mp[2].error())

            fitPi0(MassInner[ni])
            mpi = getPi0Params(MassInner[ni])
            print "Inner:", mpi

            fitPi0(MassMiddle[ni])
            print "Middle:",getPi0Params(MassMiddle[ni])

            fitPi0(MassOuter[ni])
            print "Outer:",getPi0Params(MassOuter[ni])

            MassInner[ni].Write()
            MassMiddle[ni].Write()
            MassOuter[ni].Write()
            Mass[ni].Write()

        Conv['Pass%i'%j].Write()
        Sigm['Pass%i'%j].Write()

    histofile.Close()
    os.system('chmod a+rwx '+oh)

    lmap = LambdaMap.LambdaMap()
    lmap.read(pt.lambdas_location_it()%(1,1))

    lmap2 = LambdaMap.LambdaMap()
    lmap2.read(pt.lambdas_location_it()%(n_secs,nit))
    #lmap2.read(pt.lambdas_location_it()%(MaxIt/PassIt,3))

    lams  = lmap.lambdas()
    lams2 = lmap2.lambdas()

    ROOT.gStyle.SetPalette(1)

    It12distr       = ROOT.TH1F('It12distr'      ,'Calibration constants: All   ',100,0.5,1.5)
    It12distrInner  = ROOT.TH1F('It12distrInner' ,'Calibration constants: Inner' ,100,0.5,1.5)
    It12distrMiddle = ROOT.TH1F('It12distrMiddle','Calibration constants: Middle',100,0.5,1.5)
    It12distrOuter  = ROOT.TH1F('It12distrOuter' ,'Calibration constants: Outer' ,100,0.5,1.5)

    It1distr        = ROOT.TH1F('It1distr'       ,'Calibration constants: All'   ,100,0.5,1.5)
    It1distrInner   = ROOT.TH1F('It1distrInner'  ,'Calibration constants: Inner' ,100,0.5,1.5)
    It1distrMiddle  = ROOT.TH1F('It1distrMiddle' ,'Calibration constants: Middle',100,0.5,1.5)
    It1distrOuter   = ROOT.TH1F('It1distrOuter'  ,'Calibration constants: Outer' ,100,0.5,1.5)

    It2distr        = ROOT.TH1F('It2distr'       ,'Calibration constants: All'   ,100,0.5,1.5)
    It2distrInner   = ROOT.TH1F('It2distrInner'  ,'Calibration constants: Inner' ,100,0.5,1.5)
    It2distrMiddle  = ROOT.TH1F('It2distrMiddle' ,'Calibration constants: Middle',100,0.5,1.5)
    It2distrOuter   = ROOT.TH1F('It2distrOuter'  ,'Calibration constants: Outer' ,100,0.5,1.5)

    comp2DFaceInner = ROOT.TH2F('comp2DFaceInner','',48,-970,970,36,-725,725)
    comp2DFaceInner.SetMinimum(0.5)
    comp2DFaceInner.SetMaximum(1.5)
    comp2DFaceMiddle = ROOT.TH2F('comp2DFaceMiddle','',64,-1940,1940,40,-1210,1210)
    comp2DFaceMiddle.SetMinimum(0.5)
    comp2DFaceMiddle.SetMaximum(1.5)
    comp2DFaceOuter = ROOT.TH2F('comp2DFaceOuter','',64,-3880,3880,52,-3150,3150)
    comp2DFaceOuter.SetMinimum(0.5)
    comp2DFaceOuter.SetMaximum(1.5)

    lamsa = {}

    cfs = os.path.join(pt.store_location(),'CalibCoefficients.txt')
    filtxt = open(cfs,'w')
    for ic in lams.keys():
        if not lams2.has_key(ic):
            lams2[ic] = 1.
            print ic
        lamsa[ic] = lams[ic]*lams2[ic]
        print >> filtxt, ic.index(), lamsa[ic]

    for ic2 in lams2.keys():
        if ic2 in lamsa.keys(): continue
        print ic2
        lamsa[ic2] = lams2[ic2]
        print >> filtxt, ic2.index(), lamsa[ic2]
    filtxt.close()

    os.system('chmod a+rwx '+cfs)

    for ic in lamsa.keys():
         if ic.area() == 2:
             #print ic.col(),ic.row()
             comp2DFaceInner.SetBinContent(ic.col()-7,ic.row()-13,lamsa[ic])
             if ic in lams2.keys():
                 It2distr.Fill(lams2[ic])
                 It2distrInner.Fill(lams2[ic])
             if ic in lams.keys():
                 It1distr.Fill(lams[ic])
                 It1distrInner.Fill(lams[ic])
             It12distr.Fill(lamsa[ic])
             It12distrInner.Fill(lamsa[ic])
         if ic.area() == 1:
             #print ic.col(),ic.row()
             comp2DFaceMiddle.SetBinContent(ic.col()+1,ic.row()-11,lamsa[ic])
             if ic in lams2.keys():
                 It2distr.Fill(lams2[ic])
                 It2distrMiddle.Fill(lams2[ic])
             if ic in lams.keys():
                 It1distr.Fill(lams[ic])
                 It1distrMiddle.Fill(lams[ic])
             It12distr.Fill(lamsa[ic])
             It12distrMiddle.Fill(lamsa[ic])
         if ic.area() == 0:
             #print ic.col(),ic.row()
             comp2DFaceOuter.SetBinContent(ic.col()+1,ic.row()-5,lamsa[ic])
             if ic in lams2.keys():
                 It2distr.Fill(lams2[ic])
                 It2distrOuter.Fill(lams2[ic])
             if ic in lams.keys():
                 It1distr.Fill(lams[ic])
                 It1distrOuter.Fill(lams[ic])
             It12distr.Fill(lamsa[ic])
             It12distrOuter.Fill(lamsa[ic])

    It2distr      .Fit("gaus",'','',0.5,1.5)
    It2distrInner .Fit("gaus",'','',0.5,1.5)
    It2distrMiddle.Fit("gaus",'','',0.5,1.5)
    It2distrOuter .Fit("gaus",'','',0.5,1.5)

    It12distr      .Fit("gaus",'','',0.5,1.5)
    It12distrInner .Fit("gaus",'','',0.5,1.5)
    It12distrMiddle.Fit("gaus",'','',0.5,1.5)
    It12distrOuter .Fit("gaus",'','',0.5,1.5)

    It1distr       .Fit("gaus",'','',0.5,1.5)
    It1distrInner  .Fit("gaus",'','',0.5,1.5)
    It1distrMiddle .Fit("gaus",'','',0.5,1.5)
    It1distrOuter  .Fit("gaus",'','',0.5,1.5)


    oc = os.path.join(pt.store_location(),"OutputCoefficients.root")
    fillam = ROOT.TFile(oc,'recreate')
    It2distr        .Write()
    It2distrInner   .Write()
    It2distrMiddle  .Write()
    It2distrOuter   .Write()
    It12distr       .Write()
    It12distrInner  .Write()
    It12distrMiddle .Write()
    It12distrOuter  .Write()
    It1distr        .Write()
    It1distrInner   .Write()
    It1distrMiddle  .Write()
    It1distrOuter   .Write()
    comp2DFaceInner .Write()
    comp2DFaceMiddle.Write()
    comp2DFaceOuter .Write()

    fillam          .Close()

    c01inner = TCanvas("c01inner","c01inner")
    c02inner = TCanvas("c02inner","c02inner")
    c03inner = TCanvas("c03inner","c03inner")
    c04inner = TCanvas("c04inner","c04inner")
    c01inner.Divide(24,18,0,0)
    c02inner.Divide(24,18,0,0)
    c03inner.Divide(24,18,0,0)
    c04inner.Divide(24,18,0,0)
    c01middle = TCanvas("c01middle","c01middle")
    c02middle = TCanvas("c02middle","c02middle")
    c03middle = TCanvas("c03middle","c03middle")
    c04middle = TCanvas("c04middle","c04middle")
    c01middle.Divide(32,20,0,0)
    c02middle.Divide(32,20,0,0)
    c03middle.Divide(32,20,0,0)
    c04middle.Divide(32,20,0,0)
    c01outer = TCanvas("c01outer","c01outer")
    c02outer = TCanvas("c02outer","c02outer")
    c03outer = TCanvas("c03outer","c03outer")
    c04outer = TCanvas("c04outer","c04outer")
    c01outer.Divide(32,26,0,0)
    c02outer.Divide(32,26,0,0)
    c03outer.Divide(32,26,0,0)
    c04outer.Divide(32,26,0,0)
    
    for area in range(0,4):
        
        print area
        colRange = []
        if (area==0):
            colRange = range(0,64)
        if (area==1):
            colRange = range(0,64)
        if (area==2):
            colRange = range(8,56)

        rowRange = []
        if (area==0):
            rowRange = range(6,58)
        if (area==1):
            rowRange = range(12,52)
        if (area==2):
            rowRange = range(14,50)
 
        #for col in colRange:
        #print col
        
        for row in rowRange:

            if (area==0):
                if (col>=0+16 and col<0+16+32 and row>=6+16 and row<6+16+20):
                    continue
                col = col - 0
                row = row - 6
                if (col<32 and row<26):
                    c01outer.cd(col+1 + 32*(26-row-1) )
                    gStyle.SetOptStat(0)
                if (col<32 and row>=26):
                    row = row-26
                    c02outer.cd(col+1 + 32*(26-row-1) )
                    row = row+26
                    gStyle.SetOptStat(0)
                if (col>=32 and row<26):
                    col = col-32
                    c03outer.cd(col+1 + 32*(26-row-1) )
                    col = col+32
                    gStyle.SetOptStat(0)
                if (col>=32 and row>=26):
                    col = col-32
                    row = row-26
                    c04outer.cd(col+1 + 32*(26-row-1) )
                    col = col+32
                    row = row+26
                    gStyle.SetOptStat(0)
                col = col + 0
                row = row + 6
                Histos[ni][CellID('Ecal','Outer', row, col)][2].Draw()
                fitPi0(Histos[ni][CellID('Ecal','Outer', row, col)][2])
                c01outer.Update()
                c02outer.Update()
                c03outer.Update()
                c04outer.Update()
                
            if (area==1):
                if (col>=0+16 and col<0+16+32 and row>=12+8 and row<12+8+24):
                    continue
                col = col - 0
                row = row - 12
                if (col<32 and row<20):
                    c01middle.cd(col+1 + 32*(20-row-1) )
                    gStyle.SetOptStat(0)
                if (col<32 and row>=20):
                    row = row-20
                    c02middle.cd(col+1 + 32*(20-row-1) )
                    row = row+20
                    gStyle.SetOptStat(0)
                if (col>=32 and row<20):
                    col = col-32
                    c03middle.cd(col+1 + 32*(20-row-1) )
                    col = col+32
                    gStyle.SetOptStat(0)
                if (col>=32 and row>=20):
                    col = col-32
                    row = row-20
                    c04middle.cd(col+1 + 32*(20-row-1) )
                    col = col+32
                    row = row+20
                    gStyle.SetOptStat(0)
                col = col + 0
                row = row + 12
                Histos[ni][CellID('Ecal','Middle', row, col)][2].Draw()
                fitPi0(Histos[ni][CellID('Ecal','Middle', row, col)][2])
                c01middle.Update()
                c02middle.Update()
                c03middle.Update()
                c04middle.Update()

            if (area==2):
                if (col>=8+16 and col<8+32 and row>=14+12 and row<14+24):
                    continue
                col = col - 8
                row = row - 14                
                if (col<24 and row<18):
                    c01inner.cd(col+1 + 24*(18-row-1) )
                    gStyle.SetOptStat(0)
                if (col<24 and row>=18):
                    row = row-18
                    c02inner.cd(col+1 + 24*(18-row-1) )
                    row = row+18
                    gStyle.SetOptStat(0)
                if (col>=24 and row<18):
                    col = col-24
                    c03inner.cd(col+1 + 24*(18-row-1) )
                    col = col+24
                    gStyle.SetOptStat(0)
                if (col>=24 and row>=18):
                    col = col-24
                    row = row-18
                    c04inner.cd(col+1 + 24*(18-row-1) )
                    col = col+24
                    row = row+18
                    gStyle.SetOptStat(0)
                col = col + 8
                row = row + 14                
                Histos[ni][CellID('Ecal','Inner', row, col)][2].Draw()
                fitPi0(Histos[ni][CellID('Ecal','Inner', row, col)][2])

                c01inner.Update()
                c02inner.Update()
                c03inner.Update()
                c04inner.Update()

    c01inner.SaveAs("c01inner.gif")
    c02inner.SaveAs("c02inner.gif")
    c03inner.SaveAs("c03inner.gif")
    c04inner.SaveAs("c04inner.gif")
    c01middle.SaveAs("c01middle.gif")
    c02middle.SaveAs("c02middle.gif")
    c03middle.SaveAs("c03middle.gif")
    c04middle.SaveAs("c04middle.gif")
    c01outer.SaveAs("c01outer.gif")
    c02outer.SaveAs("c02outer.gif")
    c03outer.SaveAs("c03outer.gif")
    c04outer.SaveAs("c04outer.gif")
                
    os.system('chmod a+rwx '+oc)

if '__main__' == __name__ :

    if process == 'Fill':
        print "Filling the histograms"
        FillTheHistograms()
    elif process == 'Fit':
        print "Fitting the histograms"
        FitTheHistograms()
    elif process == 'Split':
        print "Preparing histograms for fitting"
        SplitHistos()
    elif process == 'Merge':
        print "Merging the histograms"
        mergeHistos()
    elif process == 'Collect':
        print "Collecting the calibration constants"
        CollectLambdas()
    elif process == 'Statistics':
        print "Preparing the resulting histograms"
        GetOutput()
    else:
        print 'Please specify the task:'
        print '"Fill" for filling the histograms'
        print '"Fit" for fitting the histograms'
